package datastructure;

import java.util.ArrayList;
import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int cols;
    private ArrayList<CellState> cellGrid;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.cols = columns;
        cellGrid = new ArrayList<CellState>();
        for (int row=0; row<rows; row++) {
            for (int col=0; col<columns; col++) {
                cellGrid.add(initialState);
            }
        }
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        areLegalIndexes(row, column); 
        cellGrid.set(indexOf(row, column), element);
    }

    @Override
    public CellState get(int row, int column) {
        areLegalIndexes(row, column);
        return cellGrid.get(indexOf(row,column));
    }

    @Override
    public IGrid copy() {
        IGrid newCopy = new CellGrid(this.rows, this.cols, null);
        for(int row = 0; row < this.rows; row++) {
            for(int col = 0; col < this.cols; col++) {
                newCopy.set(row, col, this.get(row, col));
            }
        }
        return newCopy;
    }

    /** 
     * Throws IndexOutOfBoundsException if either row or column is out of bounds for the grid object.
     */
    private void areLegalIndexes(int row, int column) { 
        if(row < 0 || row >= this.rows) {
            throw new IndexOutOfBoundsException();
        }
        if(column < 0 || column >= this.cols) {
            throw new IndexOutOfBoundsException();
        }
    }

    /** 
     * Returns the index corresponding to the given row and column values.
     * Throws IndexOutOfBoundsException if either row or column is out of bounds for the grid object.
     */
    private int indexOf(int row, int column) {
        areLegalIndexes(row, column);
        return this.cols*row + column;
    }
}
